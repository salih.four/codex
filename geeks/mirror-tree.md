# Mirror Tree

## Problem
https://practice.geeksforgeeks.org/problems/mirror-tree/1

## Solution

### Recursive
Method: Recursive <br>
Language: C++ <br>
Time complexity: O(Number of nodes) <br>
Space complexity: 0(Height of tree) <br>

```cpp
// Function fingerprint
void mirror(Node* node) 
{
     // Your Code Here
     if(node == NULL) return;
     mirror(node->right);
     mirror(node->left);
     Node * t = node->right;
     node->right = node->left;
     node->left = t;
     return;
}
```