# Check if linked list is a palindrome

## Problem
https://practice.geeksforgeeks.org/problems/check-if-linked-list-is-pallindrome/1

## Solution

### Recursive
Method: Recursive <br>
Language: C++ <br>
Time complexity: 0(n) <br>

```cpp
// Globals
Node * head_initial;
Node * cur;
bool set = false;

// Function fingerprint
/*You are required to complete this method */
bool isPalindrome(Node *head)
{
    if(!set){ head_initial = head; cur = head; set = true; }
    if(head == NULL){
        return true;
    } else {
        bool innerPal = isPalindrome(head->next);
        if(head == head_initial){ 
            // Allow globals to be reset by the recurstion for next linked list
            set = false;
        }
        if(innerPal && head->data == cur->data){
            cur = cur->next;
            return true;
        } else return false;
    }
    
}
```