# Delete linked list node without head pointer

## Problem
https://practice.geeksforgeeks.org/problems/delete-without-head-pointer/1

## Solution

### Obvious
Language: C++ <br>
Time complexity: O(1) <br>

```cpp
// This function should delete node from linked list. The function
// may assume that node exists in linked list and is not last node
// node: reference to the node which is to be deleted
void deleteNode(Node *node)
{
    struct Node * temp = node->next;
    node->data = temp->data;
    node->next = temp->next;
    free(temp);
   // Your code here
}
```