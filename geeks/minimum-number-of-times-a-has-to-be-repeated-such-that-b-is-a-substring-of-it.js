const readline = require('readline');
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

class Node {
    v;
    n;
    p;
    constructor(_v, _n, _p){
        this.v = _v;
        this.n = _n;
        this.p = _p;
    }
}

let a, b;
rl.question("Input a: ", (answer) => {
    a = answer;
    rl.question("Input b: ", (answer) => {
        b = answer;
        res = howMany(a, b);
        if(res == -1) console.log("Impossible");
        else console.log(res);
    })
})

function howMany(a, b){
    // Making a circular doubly linked list with letters in b
    head = new Node(b[0], null, null);
    cur = head;
    for(let i=1;i<b.length;i++){
        cur.n = new Node(b[i], null, cur);
        cur = cur.n;
    }
    cur.n = head;
    head.p = cur;

    cur = head;
    res = 0
    stop = head.p;
    if(a[0] != head.v){
        let i;
        for(i =1;i<a.length;i++){
            cur = cur.n
            console.log(cur.v);
            if(cur.v == a[0]){
                res = res+2;
                break;
            }
        }
        if(i == a.length) return -1;
    }
    while(true){
        for(let i=0;i<a.length;i++){
            if(a[i]!=cur.v){
                return -1;
            }
            cur = cur.n
            if(cur == stop) return ++res;
        }
        res++;
    }
}