# Mark and Toys

## Problem
https://www.hackerrank.com/challenges/mark-and-toys/problem

## Solution

### Bubble Sort
Method: Partial sorting with the bubble sort technique <br>
Language: C++ <br>
Time complexity: O((Length of Array)<sup>2</sup>) <br>
Space complexity: Constant <br>
Test result: 17/17 <br>

```cpp
// Fingerprint function
int maximumToys(vector<int> prices, int k) {
    int n = prices.size();
    int number = 0;
    int price = 0;
    while(true){
        for(int i=n-1;i>0;i--){
            if(prices[i] < prices[i-1]){
                int t = prices[i-1];
                prices[i-1] = prices[i];
                prices[i] = t;
            }
        }
        if(price + prices[number] < k){
            price += prices[number];
            number++;
        } else {
            break;
        }
    }
    return number;
}
```