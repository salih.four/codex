# Alternating Characters

## Problem
https://www.hackerrank.com/challenges/alternating-characters/problem <br>
Difficulty: Easy

## Solution

### Iterative
Method: Iteratively eliminate adjasent duplicates inplace <br>
Language: C++ <br>
Time complexity: O((Length of Array)<sup>2</sup>) <br>
Space complexity: Constant <br>
Test result: 14/14 <br>

```cpp
// Fingerprint function
int alternatingCharacters(string s) {
    int n = s.length();
    int n_init = n;
    int i = n-1;
    while(true){
        if(s[i] == s[i-1]){
            s.erase(i, 1);
            n--;
        } else {
            i--;
        }
        if(i == 0) break;
    }
    return n_init - n;
}
```