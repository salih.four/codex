# Sherlock and Anagrams

## Problem
https://www.hackerrank.com/challenges/sherlock-and-anagrams/problem

## Solutions
### Brute force
Method: Brute Force <br>
Language: Python 3 <br>
Test result: 7/7

```py
def isAnagram(a, b):
    a = sorted(a)
    b = sorted(b) 
    for i in range(len(a)):
        if a[i] != b[i]:
            return False
    return True

# Fingerprint function   
def sherlockAndAnagrams(s):
    count = 0
    for n in range(len(s)):
        for i in range(len(s)-(n+1)):
            for j in range(i+1, len(s)-(n)):
                if isAnagram(s[i:i+n+1], s[j:j+n+1]):
                    count += 1
    return count
```