# Count Triplets

## Problem
https://www.hackerrank.com/challenges/count-triplets-1/problem

## Solution

### Brute Force
Method: Brute force <br>
Language: Python <br>
Time complexity: O((Length of Array)<sup>3</sup>) <br>
Space complexity: Constant <br>
Test cases: 5/12 passed <br>
Errors: Timeout reached <br>

```py
# Function fingerprint
def countTriplets(arr, r):
    count = 0    
    n = len(arr)
    for i in range(n):
        for j in range(i+1, n):
            if r == arr[j] / arr[i]:
                count += arr[j+1:].count(arr[j]*r)
    return count
```