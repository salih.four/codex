var markEvent = function(tracker_name, event, callback){
    var req = new XMLHttpRequest();
    console.log(callback);
    req.onreadystatechange = function(){
        if(req.readyState === 4) {
            if(callback) callback();
        }
    }
    req.open("POST", '//blog.muhammedsalih.com/analytics/trackers/mark');
    req.setRequestHeader('Content-type', 'application/json');
    req.send(JSON.stringify({tracker_name,event}));
};

document.onreadystatechange = function(){
    if(document.readyState == "interactive"){
        markEvent("Codex Website", "visit");
    }
}