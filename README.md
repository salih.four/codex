# Hackerrank
- [Sherlock and Anagrams](./hackerrank/sherlock-and-anagrams)
- [Count Triplets](./hackerrank/count-triplets.md)
- [Mark and Toys](./hackerrank/mark-and-toys.md)
- [Alternating Characters](./hackerrank/Alternating%20Characters.md)

# Geeks for Geeks
- [Sum of leaf nodes in BST](./geeks/sum-of-leaf-nodes-in-bst.md)
- [Delete without head pointer](./geeks/delete-without-head-pointer.md)
- [Check if linked list is palindrome](./geeks/check-if-linked-list-is-palindrome.md)
- [Mirror Tree](./geeks/mirror-tree.md)